# Simple python script to combine 2 csv files
import pandas as pd


def combine_csv(file1, file2):
    df1 = pd.read_csv(file1)
    df2 = pd.read_csv(file2)
    combined_df = pd.concat([df1, df2])
    return combined_df.to_csv("combined.csv")


combine_csv("/home/anurag/Documents/Projects/RandomNameGenerator/dc-wikia-data.csv",
            "/home/anurag/Documents/Projects/RandomNameGenerator/marvel-wikia-data.csv")


