from random import randint
from utils.error import error_message


def random_number_generator(lower_limit, upper_limit):
    """
    Generates a random number between the lower and upper limit
    :param lower_limit
    :param upper_limit
    :return: Generated random number
    """
    try:
        return randint(lower_limit, upper_limit)
    except ValueError:
        return error_message("LimitError", "random_number_generator(): Range argument passed invalid")


def two_digit_random_number():
    """
    Generate a 2 digit random number
    :return: Return a 2 digit random number
    """
    number = randint(0, 99)
    return "0%d" % number if number < 10 else "%d" % number


def boolean_random():
    """
    Generate a boolean random decision
    :return: Random Number
    """
    return randint(0, 1)
