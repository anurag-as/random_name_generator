from utils.error import error_message


def file_len(file_name):
    """
    Count number of files in a line
    :param file_name: Name of file
    :return: Number of lines -> int
    """
    try:
        with open(file_name) as f:
            for i, line in enumerate(f):
                pass
            return i + 1
    except FileNotFoundError:
        return error_message("FileNotFoundError", "file_len(): Invalid file name")


def read_line(file_name, line):
    """
    Read a particular line in a file
    :param file_name: Name of file
    :param line: Line Number
    :return: Data string in that line
    """
    try:
        with open(file_name) as f:
            for i, data in enumerate(f):
                if i+1 == line:
                    return data
            return error_message("LineError", "read_line(): Invalid line number")
    except FileNotFoundError:
        return error_message("FileNotFoundError", "file_len(): Invalid file name")


