# python function to get cartoon names in a single file
import pandas as pd


def save_to_file(names):
    f = open("cartoon_names.txt", "w")
    for name in names:
        f.write(name + "\n")


def get_cartoon_names(file):
    df = pd.read_csv(file)
    names = df['name'].apply(lambda x: x.split('(')[0]).to_list()
    names_unflattened = [x.strip().split(' ') for x in names]
    names_flattened = [x.lower() for sublist in names_unflattened for x in sublist]
    save_to_file(names_flattened)

