from utils.randomizer import random_number_generator, two_digit_random_number, boolean_random
from utils.file_functions import file_len, read_line


class RNG:
    def __init__(self):
        """
        Initialization of variables for class Random Name Generator
        """
        self._name = ""
        self._cartoon_name = ""
        self._cartoon_file = ".//utils//cartoon_names.txt"
        self._number = ""

    def __get_cartoon_name(self):
        """
        Function to get a cartoon name
        :return: Cartoon name
        """
        upper_limit = int(file_len(self._cartoon_file))
        random_number = random_number_generator(1, upper_limit)
        self._cartoon_name = read_line(self._cartoon_file, random_number).strip()

    def __get_random_number(self):
        """
        Function to get a random number
        :return: 2 digit random number
        """
        self._number = two_digit_random_number()

    def __random_name_generator(self):
        """
        Randomize name
        :return: None
        """
        self.__get_cartoon_name()
        self.__get_random_number()
        decision = boolean_random()
        self._name = self._cartoon_name + self._number if decision else self._number + self._cartoon_name

    def get_name(self):
        """
        Exposed function to return random name
        :return: Returns random name
        """
        self.__random_name_generator()
        return self._name


r = RNG()
print(r.get_name())